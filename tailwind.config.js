module.exports = {
  presets: [
    require('../extreme-theme/tailwind.config')
  ],
  important: true,
  content: [
    '../extreme-theme/**/*.php',
    './**/*.php',
    './tailwind.safelist.txt',
  ],
}
